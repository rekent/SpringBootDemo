package com.rekent.springboot.startdemo.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "askTime")
public class Receiver {
	
	@RabbitHandler
    public void process(String hello) {
        System.out.println("Receiver : " + hello);
    }

}
