package com.rekent.springboot.startdemo.rabbitmq;

import java.util.Date;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Sender {
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	
	public void send() {
		String currentTime=new Date().toString();
		rabbitTemplate.convertAndSend("askTime",currentTime);
		System.out.println("RabbitMQ Sender Send:\""+currentTime);
	}

}
