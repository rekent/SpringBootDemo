package com.rekent.springboot.startdemo.domain;

import org.springframework.stereotype.Component;

@Component
public class MetaDataInfo {
	String uuid;
	String size;
	String createDate;
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	

}
