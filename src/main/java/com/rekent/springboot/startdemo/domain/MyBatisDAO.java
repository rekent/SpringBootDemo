package com.rekent.springboot.startdemo.domain;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface MyBatisDAO {
	
	@Select("Select * from tbl_metadata")
	List<MetaDataInfo> listall();
}
