package com.rekent.springboot.startdemo.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface JPADAO extends JpaRepository<Userinfo, Long>{
	
	Userinfo findByUser(String user);
	
	@Query("from Userinfo u where u.user=:username")
	Userinfo finduser(String username);
}
