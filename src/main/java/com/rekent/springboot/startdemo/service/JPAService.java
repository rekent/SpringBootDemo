package com.rekent.springboot.startdemo.service;

import com.rekent.springboot.startdemo.domain.Userinfo;

public interface JPAService {

	public Userinfo search(String user);
	
	public Userinfo searchQuery(String user);
}
