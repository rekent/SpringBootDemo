package com.rekent.springboot.startdemo.service;

public interface EchoService {
	
	public String echo(String sayhi);
	
}
