package com.rekent.springboot.startdemo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import com.rekent.springboot.startdemo.domain.MetaDataInfo;
import com.rekent.springboot.startdemo.domain.MyBatisDAO;
import com.rekent.springboot.startdemo.service.MyBatisService;

@Service
@ComponentScan(basePackages= {"com.rekent.springboot.startdemo.dao"})
public class MyBatisServiceImpl implements MyBatisService {

	@Autowired
	private MyBatisDAO dao;
	
	@Override
	public List<MetaDataInfo> listall() {
		return dao.listall();
	}

}
