package com.rekent.springboot.startdemo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.rekent.springboot.startdemo.domain.Userinfo;
import com.rekent.springboot.startdemo.service.JDBCService;

@Service
public class JDBCServiceImpl implements JDBCService{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Userinfo add(Userinfo userinfo) {
		jdbcTemplate.update("insert into tbl_user (user,age,sex) values(?,?,?)",
				userinfo.getUser(),userinfo.getAge(),userinfo.getSex());
		return userinfo;
	}

}
