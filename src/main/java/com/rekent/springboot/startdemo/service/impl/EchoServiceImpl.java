package com.rekent.springboot.startdemo.service.impl;

import org.springframework.stereotype.Service;

import com.rekent.springboot.startdemo.service.EchoService;

@Service
public class EchoServiceImpl implements EchoService{

	@Override
	public String echo(String sayhi) {
		return "Server echo:  "+sayhi+" 's request.";
	}
	
	
}
