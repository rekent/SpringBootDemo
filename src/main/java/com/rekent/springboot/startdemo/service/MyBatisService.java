package com.rekent.springboot.startdemo.service;

import java.util.List;

import com.rekent.springboot.startdemo.domain.MetaDataInfo;

public interface MyBatisService {
	List<MetaDataInfo> listall();
}
