package com.rekent.springboot.startdemo.service;

import com.rekent.springboot.startdemo.domain.Userinfo;

public interface JDBCService {

	public Userinfo add(Userinfo userinfo);
}
