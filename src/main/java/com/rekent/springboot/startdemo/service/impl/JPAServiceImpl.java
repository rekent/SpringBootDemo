package com.rekent.springboot.startdemo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rekent.springboot.startdemo.domain.JPADAO;
import com.rekent.springboot.startdemo.domain.Userinfo;
import com.rekent.springboot.startdemo.service.JPAService;

@Service
public class JPAServiceImpl implements JPAService {
	
	@Autowired
	JPADAO dao;
	
	@Override
	public Userinfo search(String user) {
		return dao.findByUser(user);
	}
	
	@Override
	public Userinfo searchQuery(String user) {
		return dao.finduser(user);
	}

}
