package com.rekent.springboot.startdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rekent.springboot.startdemo.domain.MetaDataInfo;
import com.rekent.springboot.startdemo.service.MyBatisService;

@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages= {"com.rekent.springboot.startdemo.service"})
public class MyBatisController {
	
	@Autowired
	private MyBatisService Service;
	
	@RequestMapping(value="/listall")
	public List<MetaDataInfo> listall(){
		return Service.listall();
	}
}
