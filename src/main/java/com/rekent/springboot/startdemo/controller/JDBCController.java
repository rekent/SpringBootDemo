package com.rekent.springboot.startdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rekent.springboot.startdemo.domain.Userinfo;
import com.rekent.springboot.startdemo.service.JDBCService;

@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages= {"com.rekent.springboot.startdemo.service"})
public class JDBCController {
	@Autowired
	JDBCService service;
	
	@RequestMapping(value="/add")
	public Userinfo add(Userinfo userinfo) {
		return service.add(userinfo);
	}
}
