package com.rekent.springboot.startdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rekent.springboot.startdemo.domain.Userinfo;
import com.rekent.springboot.startdemo.service.JPAService;

@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages= {"com.rekent.springboot.startdemo.service"})
public class JPAController {
	
	@Autowired
	JPAService service;
	
	@RequestMapping(value="/search")
	public Userinfo search(String user) {
		return service.search(user);
	}
	
	@RequestMapping(value="/searchQuery")
	public Userinfo searchQuery(String user) {
		return service.searchQuery(user);
	}
}
