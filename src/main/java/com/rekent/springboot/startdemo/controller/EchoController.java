package com.rekent.springboot.startdemo.controller;


import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rekent.springboot.startdemo.service.EchoService;


@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.rekent.springboot.startdemo.service"})
public class EchoController {
	private static final Logger logger=LoggerFactory.getLogger(EchoController.class);
	@Autowired
	private EchoService service;
		
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		logger.info("Reciving  "+request.getRemoteAddr()+"'s Request!");
		return service.echo(request.getRemoteHost());
	}
}
